module.exports = {
  theme: {
    screens: {
      // You can change your breakpoints below if you'd like.
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
    },
    container: {
      center: true,
    },
    extend: {
      // Extend existing configuration here, i.e. height, width and fontSize
    }
  },
  variants: {},
  plugins: []
}
