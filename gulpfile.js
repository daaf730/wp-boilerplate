const browsersync = require('browser-sync').create();
const csso = require('gulp-csso');
const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const postcss = require('gulp-postcss');
const concat = require('gulp-concat');
const terser = require('gulp-terser');
const newer = require('gulp-newer');
const purgecss = require('gulp-purgecss');
const rename = require('gulp-rename');

// List all the files that contain CSS below (and if you're uncertain, just list it below. It won't hurt.)
const purgeFiles = [
  './404.php', 
  './archive.php',
  './archive-*.php',
  './footer.php',
  './footer-*.php',
  './front-page.php',
  './functions.php',
  './header.php',
  './header-*.php',
  './home.php',
  './index.php', 
  './page.php',
  './single.php', 
  './single-*.php', 
  './page-templates/*.php', 
  './content/*.php',
  './content/sections/*.php',
  './src/js/*.js'
];

// BrowserSync
function browserSync(done) {
    browsersync.init({
        browser: 'Chrome',
        proxy: {
          target: 'localhost/',
          proxyRes: [
            function (res) {
                res.headers["Expires"] = "0";
                res.headers["Cache-Control"] = "no-cache, no-store, must-revalidate";
                res.headers["Pragma"] = "no-cache";
            }
        ]
        },
        notify: false
    });
    done();
  }
  
  // BrowserSync Reload
  function browserSyncReload(done) {
    browsersync.reload();
    done();
  }

  // Optimize Images
function images() {
    return gulp
      .src("./src/img/*")
      .pipe(newer("./dist/img"))
      .pipe(
        imagemin([
          imagemin.gifsicle({ interlaced: true }),
          imagemin.jpegtran({ progressive: true }),
          imagemin.optipng({ optimizationLevel: 5 }),
          imagemin.svgo({
            plugins: [
              {
                removeViewBox: false,
                collapseGroups: true
              }
            ]
          })
        ])
      )
      .pipe(gulp.dest("./dist/img"));
    }

// Purge CSS
function purgeCSS() {
  return gulp.src('./dist/css/tailwind.scss')
    .pipe(purgecss({
        content: purgeFiles
    }))
}

// Compile Tailwind + Minifying the output
function css() {

  var tailwindcss = require('tailwindcss');

    // Compile Tailwind
    return gulp.src('./src/css/tailwind.scss')
      .pipe(postcss([
        tailwindcss('./tailwind.config.js'),
        require('autoprefixer')
      ]))
      // Storing the complete tailwind.css for fallback purposes
      .pipe(gulp.dest('./dist/css'))
      // Purge CSS (Recommend to comment the purge lines below in development)
      .pipe(purgecss({ 
        content: purgeFiles,
        defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
      }))
      // Minify CSS
      .pipe(csso())
      // Renaming tailwind.css to style.css
      .pipe(rename("style.css"))
      .pipe(gulp.dest('./'))
}
 
// Compress JS
function js(done) {
  destination = './dist/js'
  
  return gulp.src('./src/js/*.js')
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(destination))
    .pipe(terser())
    .pipe(rename('scripts.min.js'))
    .pipe(gulp.dest(destination))
}

// Watch files
function watchFiles() {
    gulp.watch("./src/css/*", gulp.series(css, browserSyncReload));
    gulp.watch("./src/js/*", gulp.series(js, browserSyncReload));
    gulp.watch(      
        [
        "./*.php",
        "./page-templates/*.php",
        "./sections/*php",
        "./sections/**/*.php"
      ],
      browserSyncReload
    );
    gulp.watch("./src/img/*", images);
    }

// Define complex tasks
const compile = gulp.series(css, js, images);
const watch = gulp.parallel(watchFiles, browserSync);

// Export tasks
exports.purgecss = purgeCSS;
exports.css     = css;
exports.images  = images;
exports.js      = js;
exports.watch   = watch;
exports.default = compile;
