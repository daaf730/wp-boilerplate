<?php
/*
 *  Author: 730.
 *  The index of all content sections
 */
?>
<?php 
// Check value exists.
if( have_rows('contentsecties') ):

      // Loop through rows.
      while ( have_rows('contentsecties') ) : the_row();

            // Page intro
            if( get_row_layout() == 'intro' ):
                  include (get_template_directory() . '/content/home/intro.php');

            // Another section
            elseif( get_row_layout() == 'another-section' ):
                  include (get_template_directory() . '/content/about/another-section.php');

            endif;

      // End loop.
      endwhile;

// No value.
else :
    echo "No content sections found...";
endif;
?>
