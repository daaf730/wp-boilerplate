# This is the 730. Wordpress Boilerplate
People may use this boilerplate to get an extremely minimalist Wordpress theme to start their own from scratch.

# Build system
* [Gulp](https://github.com/gulpjs/gulp)

# CSS
* TailwindCSS

# Optimization
* Uglify
* Imagemin
* CSSO
* PurgeCSS
* Concat
* Terser

# Automatic reloading
* Browser-sync

# Prerequisites
This boilerplate requires [Node.js](https://nodejs.org/en/), [NPM](https://www.npmjs.com/get-npm) or [Yarn](https://yarnpkg.com/en/docs/install#windows-stable) and [Gulp](https://github.com/gulpjs/gulp) to be installed.

# Installation
* Open Terminal in your /wp-content/themes path and execute "git clone https://gitlab.com/daaf730/wp-boilerplate.git"
* CD into the wp-boilerplate folder
* Run npm install / yarn install
* Done

# Commands
* `gulp css` to run the css job
* `gulp js` to run the js job
* `gulp images` to optimize images from the src/img and place them in the dist/img
* `gulp purgecss` to run the purgeCSS job
* `gulp watch` to watch all the files and run the according jobs with a browser reload after it