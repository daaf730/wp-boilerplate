function docReady(fn) {
      // see if DOM is already available
      if (document.readyState === "complete" || document.readyState === "interactive") {
            // call on next available tick
            setTimeout(fn, 1);
      } else {
            document.addEventListener("DOMContentLoaded", fn);
      }
} 

docReady(function() {
      var userfield = document.querySelector('#user_login');
      var passfield = document.querySelector('#user_pass');

      userfield.placeholder = "Gebruikersnaam of e-mail";
      passfield.placeholder = "Wachtwoord";
})


